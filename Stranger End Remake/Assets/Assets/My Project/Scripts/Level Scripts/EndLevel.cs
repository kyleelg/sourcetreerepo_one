﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndLevel : MonoBehaviour
{
    private Scene scene;
    public float restartDelay = 5f;         // Time to wait before restarting the level
    float restartTimer;                     // Timer to count up to restarting the level

    void Start()
    {
        scene = SceneManager.GetActiveScene();
    }

    //Load next level 
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            // .. then reload the currently loaded level. or whichever the next level is. 
            SceneManager.LoadScene("DemoMapStranger", LoadSceneMode.Single);
        }
    }
}