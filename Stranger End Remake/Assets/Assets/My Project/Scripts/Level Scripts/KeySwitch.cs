﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeySwitch : MonoBehaviour
{
    public enum Keyswitch { RedKey, TurqoiseKey, YellowKey }; //Key names for each door. enum gives key a types
    public Keyswitch whatKeyAmI;
}
