﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Scripting was based on Bonesaw Script From Richard Lago: KSU 2019 Global Game Jam 2019 "Where the Heart is not"
//Scripting Re-implemented and Redesigned by Kyleel Goings
public class SidetoSideWall : MonoBehaviour
{
    public Vector3 moveSpeed;
    public float patrolLength;
    public float patrolStep;
    public bool Switch;

    void LateUpdate()
    {
        if (Switch == true)
        {
            SidetoSide();
        }
    }
    //Pre-Condition the Player activates A Switch. 
    void SidetoSide()
    {
        if (patrolStep > patrolLength)
        {
            patrolStep = 0;
            moveSpeed *= -1;
        }
        gameObject.transform.Translate(moveSpeed * Time.deltaTime);
        patrolStep += moveSpeed.magnitude * Time.deltaTime;

    }
  
}

