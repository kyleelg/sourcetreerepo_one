﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class EndGame : MonoBehaviour
{
    public float restartDelay = 5f;         // Time to wait before restarting the level
    Text anim;                          // Reference to the animator component.
    float restartTimer;                     // Timer to count up to restarting the level

    void Awake()
    {
        // Set up the reference.
        anim = GetComponent<Text>();
    }

    // FadeInOut

    //End Game 
    void OnTriggerEnter(Collider other)
    {
            

        if (other.gameObject.tag == "Player")
        {

            //Fade into black 
            /**************************/
            // .. increment a timer to count up to restarting.
            restartTimer += Time.deltaTime;
            /**************************/

            //Bring up white text
            /**************************/
            // ... tell the animator the game is over.
            //anim.SetTrigger("Thanks for Playing. ");
            /**************************/
            // .. if it reaches the restart delay...
            if (restartTimer >= restartDelay)
            {
                //End Game 
                if (Input.GetKey("enter"))
                {
                    // .. then reload the currently loaded level. or whichever the next level is. 
                    Application.Quit(); //Quit game 
                }
            }
        }
    }
}