﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorManager : MonoBehaviour
{
    public enum Doors { RedDoor, TurqoiseDoor, YellowDoor }; //Key names for each door. enum gives key a types
    public Doors whatDoorAmI;
}
