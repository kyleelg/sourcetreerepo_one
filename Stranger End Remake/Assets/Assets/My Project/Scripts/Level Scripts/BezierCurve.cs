﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/************************************************/
/* This Script is to make the keys in the       */
/* game follow the route pathing.               */
/************************************************/

public class BezierCurve : MonoBehaviour
{
    [SerializeField]
    private Transform[] Routes;
    private int routeToTravel; //Holds the Controlpoint indexes
    private float tParam;
    private Vector3 KeyPos; //position of the floating key
    private float Speed; //Speed the key is traveling
    private bool coroutineAllowed;

    // Start is called before the first frame update
    void Start()
    {
        routeToTravel = 0;
        tParam = 0f;
        Speed = 0.5f;
        coroutineAllowed = true;
    }

    // Update is called once per frame
    void Update()
    {
        //This will cause the Key to rotate in place
        //transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);

        if (coroutineAllowed)
        {
            StartCoroutine(TakeRoute(routeToTravel));
        }   
    }

    private IEnumerator TakeRoute(int routeNum)
    {
        coroutineAllowed = false;

        Vector3 ControlPoint0 = Routes[routeNum].GetChild(0).position;
        Vector3 ControlPoint1 = Routes[routeNum].GetChild(1).position;
        Vector3 ControlPoint2 = Routes[routeNum].GetChild(2).position;
        Vector3 ControlPoint3 = Routes[routeNum].GetChild(3).position;

        while (tParam < 1)
        {
            tParam += Time.deltaTime * Speed;

            KeyPos = Mathf.Pow(1 - tParam, 3) * ControlPoint0 + 3 * Mathf.Pow(1 - tParam, 2)
            * tParam * ControlPoint1 + 3 * (1 - tParam) * Mathf.Pow(tParam, 2) * ControlPoint2 +
            Mathf.Pow(tParam, 3) * ControlPoint3;

            transform.position = KeyPos;
            yield return new WaitForEndOfFrame();
        }

        tParam = 0f;

        routeToTravel += 1;

        if (routeToTravel > Routes.Length - 1)
            routeToTravel = 0;

        coroutineAllowed = true;
        
       
    }
}
