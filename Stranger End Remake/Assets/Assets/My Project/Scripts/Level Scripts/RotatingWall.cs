﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Scripting was based on CicularMovement Script From Richard Lago: KSU 2019 Global Game Jam 2019 "Where the Heart is not"
//Scripting Re-implemented and Redesigned by Kyleel Goings
public class RotatingWall : MonoBehaviour
{

    public float moveSpeed; // Speed at which the Object moves 
    public float angle; //The angle  of the object 
    public float radius; //The objects radius 
    private Vector3 center; //The center of the object 
    Vector3 moveDirection; //The direction the roatating object will travel

    private void Start()
    {
        center = transform.position; //The Object Position at Start is its origin. 
    }

    void Update()
    {
        angle += moveSpeed * Time.deltaTime;

        moveDirection = new Vector3(Mathf.Sin(angle), 0, Mathf.Cos(angle)) * radius;
        transform.position = center + moveDirection;
    }

    private void OnCollisionStay(Collision collision)
    {
        collision.gameObject.GetComponent<Rigidbody>().AddForce(moveDirection * 10 * moveSpeed);
    }
}
