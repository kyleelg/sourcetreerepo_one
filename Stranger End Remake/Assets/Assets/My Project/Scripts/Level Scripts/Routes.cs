﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/************************************************/
/* This Script is to make the keys in the       */
/* game float in a Bezier curve.                */
/* This will hold the path the curve will follow*/
/************************************************/

public class Routes : MonoBehaviour
{

    [SerializeField]
    private Vector3 gizmosPosition;
    //Access The Controll points on the table
    private Transform[] ControlPoints;


    //This function will hold the moving path for the key.
    private void OnDrawGizmos()
    {
        /*
        //This will cause the the Object to move along the curve. 
        for (float i = 0; i <= 1; i += 0.05f)
        {
            //Move along route one way 
            gizmosPosition = Mathf.Pow(1 - i, 3) * ControlPoints[0].position + 3 * Mathf.Pow(1 - i, 2) * i * ControlPoints[1].position + 3 * 
                (1 - i) * Mathf.Pow(i, 2) * ControlPoints[2].position + Mathf.Pow(i, 3) * ControlPoints[3].position;

            Gizmos.DrawSphere(gizmosPosition, 0.25f); //Draws a Sphere
        }

        Gizmos.DrawLine(new Vector3(ControlPoints[0].position.x,ControlPoints[0].position.y,ControlPoints[0].position.z),
        new Vector3 (ControlPoints[1].position.x, ControlPoints[1].position.y, ControlPoints[1].position.z));

        Gizmos.DrawLine(new Vector3(ControlPoints[2].position.x,ControlPoints[2].position.y, ControlPoints[2].position.z),
        new Vector3 (ControlPoints[3].position.x, ControlPoints[3].position.y, ControlPoints[3].position.z));
        */
    }
}
