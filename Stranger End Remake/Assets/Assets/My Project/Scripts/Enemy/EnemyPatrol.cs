﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyPatrol : MonoBehaviour
{
    public Transform[] Patrolpoints; //The array of Patrol Points for the enemy to travel between. 
    private int destPoint = 0; //The Enemies Destination.
    private NavMeshAgent agent; //The Nav Agent 
    public Transform player, ai, target; //The Player 
    public float distanceToSee = 4; //The Distance the Enemy Can see.
    RaycastHit whatIHit; //What the Enemy can see.
    float distance = 100.0f; // how far they can see the target
    float arc = 45.0f; // their field of view


    void Start()
    {
        agent = GetComponent<NavMeshAgent>(); //Find Nav Agent 
        player = GameObject.FindWithTag("player").transform; //Find the player 
        // Disabling auto-braking allows for continuous movement
        // between points (ie, the agent doesn't slow down as it
        // approaches a destination point).
        agent.autoBraking = false;
        GotoNextPoint(); //Keep Patroling to next point.  
    }


    void GotoNextPoint()
    {
        // Returns if no points have been set up
        if (Patrolpoints.Length == 0)
            return;

        // Set the agent to go to the currently selected destination.
        agent.destination = Patrolpoints[destPoint].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % Patrolpoints.Length;
    }


    void Update()
    {
        // Choose the next destination point when the agent gets
        // close to the current one.
        if (!agent.pathPending && agent.remainingDistance < 0.5f)
        {
            GotoNextPoint();
        }

        

        if (Vector3.Distance(ai.position, player.position) < distance)
        {
            //If enemy Raycast hits Player 
            if (Vector3.Dot(ai.forward, player.position) > 0 && Vector3.Angle(ai.forward, player.position) < arc)
            {
                // enemy is ahead of me and in my field of view
                // do your raycast
                FollowPlayer();
            }
            /*
            //If enemy Raycast hits Hiding spot. 
            if (whatIHit.collider.tag == "hidingspot")
            {
                GotoNextPoint();
            }
            */
            else { GotoNextPoint(); }
        }
    }
   
    //When player is near a key or enemy increase intensity as they get closer and decrease intensity as the get further.
    void FollowPlayer()
    {
        //Walk towards player. 
        // Set the agent to go to the currently selected player
        while (Vector3.Distance(ai.position, player.position) < distance)
        {
            agent.destination = player.position;
        }
    }

    /*
    void AttackPlayer
    {
        //The Enemy Will try to captrue the soul.  
    }
    */
}
   
