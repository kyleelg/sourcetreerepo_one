﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
//DarknessAI was based on basic version of BreadcrumbAi
//Scripting Re-implemented and Redesigned by Kyleel Goings
public class DarknessAI : MonoBehaviour
{
    /*


    // private Transform SpawnPortal; //Reference to where to the Darkness will spawn
    //private CharacterAnimation anim; //Animation script to be implemented later
    private Transform Player; //Reference to Player Position
    private RaycastHit hitPlayer, left, right, forLeft, forRight;

    public bool PlayerSeen;

    // Last known Position variables
    private GameObject[] lastPositions;
    private GameObject closestPosition;
    private LayerMask lastPosLayer = (1 << 10),
                        enemyLayer = (1 << 9),
                        playerLayer = (1 << 8);
    Rigidbody rB2D;

    private float speed = 8f;

    // AI wall avoidance variables
    private float avoidDist = 2f,
                    avoidAngleDist = 2f,
                    avoidSpeed = 100f;

    void Start()
    {
        Player = GameObject.Find("Player").transform; // Find the Player GameObject Transform Position
        //anim = GetComponent<CharacterAnimation>(); // Get the animation script
        rB2D = GetComponent<Rigidbody>();

    }

    // FixedUpdate is used for physics based movement
    void FixedUpdate()
    {
        EnemyVision(); // Call the enemey vision function
        EnemyMove(); // Call the enemy movement function
        EnemyAvoid(); // Call the enemy avoidance function
    }

    void EnemyVision()
    {
        if (Physics.Linecast(transform.position, Player.position, out hitPlayer, ~(lastPosLayer))) // Linecast towards the Player ignoring the last position layer
        {
            if (hitPlayer.collider.tag == "Player") // if the raycast hits the Player then continue
            {
                PlayerSeen = true; // Player has been spotted
                Debug.DrawLine(transform.position, hitPlayer.point, Color.red); //Draw a red line from the enemy to the Player
            }
            else // If the raycast doesn't hit the Player then continue with ELSE
            {
                PlayerSeen = false; // Player has not been spotted
            }
        }
    }

    void EnemyMove()
    {
        if (PlayerSeen) // If the Player has been spotted then continue
        {
            if (Vector3.Distance(transform.position, Player.position) > 2) // If the distance from the Player is greater than a number then continue
            {
                EnemyRotate(Player.position); // Calls the rotate function sending the players position
                rB2D.MovePosition(Vector3.MoveTowards(transform.position, Player.position, Time.deltaTime * speed)); // Move the enemy towards the players position
                //anim._animRun = true; // Enable the run animation
            }
            else    // If the distance from the Player is not greater than a number then continue
            {
                //anim._animRun = false; // Disable the run animation
            }
        }
        else if (FindLastPosition() != null) // If the Player has NOT been spotted and a last position has been found then continue **FindLastPosition() returns a GameObject
        {
            EnemyRotate(FindLastPosition().transform.position); // Rotate enemy towards the found last position game object
            rB2D.MovePosition(Vector3.MoveTowards(transform.position, FindLastPosition().transform.position, Time.deltaTime * speed)); // Move the enemy towards the found last position game object
            //anim._animRun = true; // Enable the run animation
            Debug.DrawLine(transform.position, FindLastPosition().transform.position, Color.green); // Draw a green line between the enemy and the last position
        }
        else
        {
            //anim._animRun = false; // Disable the run animation
        }
    }

    private void EnemyRotate(Vector3 position)
    {
        rB2D.MoveRotation(Quaternion.LookRotation(position - transform.position)); // Rotate the enemy to look towards the players position
    }

    private void EnemyAvoid()
    {
        // Cast a line from the Ai to the left with a distance, if the line hits anything other than a last position, an enemy, or a Player, then continue
        if (Physics.Raycast(transform.position, -transform.right, out left, avoidDist, ~(lastPosLayer | enemyLayer | playerLayer)))
        {
            Debug.DrawLine(transform.position, left.point, Color.cyan); // Draw a line between the Ai position and the left hit position
            rB2D.AddForce(transform.right * avoidSpeed);    // Add force to the right
        }
        else // If the cast no is no longer hitting anything the continue
        {
            rB2D.velocity = Vector3.zero; // Stops all rigidbody movement, this removes any sliding around
        }
        // Cast a line from the Ai to the right with a distance, if the line hits anything other than a last position, an enemy, or a Player, then continue
        if (Physics.Raycast(transform.position, transform.right, out right, avoidDist, ~(lastPosLayer | enemyLayer | playerLayer)))
        {
            Debug.DrawLine(transform.position, right.point, Color.cyan);// Draw a line between the Ai position and the right hit position
            rB2D.AddForce(-transform.right * avoidSpeed); // Add force to the left
        }
        else // If the cast no is no longer hitting anything the continue
        {
            rB2D.velocity = Vector3.zero;// Stops all rigidbody movement, this removes any sliding around
        }
        // Cast a line from the Ai to its forward left with a distance, if the line hits anything other than a last position, an enemy, or a Player, then continue
        if (Physics.Raycast(transform.position, transform.forward + -transform.right * 2, out forLeft, avoidAngleDist, ~(lastPosLayer | enemyLayer | playerLayer)))
        {
            Debug.DrawLine(transform.position, forLeft.point, Color.cyan);// Draw a line between the Ai position and the forward left hit position
            rB2D.AddForce(transform.right * avoidSpeed);            // Add force to the right
        }
        else // If the cast no is no longer hitting anything the continue
        {
            rB2D.velocity = Vector3.zero;// Stops all rigidbody movement, this removes any sliding around
        }
        // Cast a line from the Ai to its forward right with a distance, if the line hits anything other than a last position, an enemy, or a Player, then continue
        if (Physics.Raycast(transform.position, transform.forward + transform.right * 2, out forRight, avoidAngleDist, ~(lastPosLayer | enemyLayer | playerLayer)))
        {
            Debug.DrawLine(transform.position, forRight.point, Color.cyan);// Draw a line between the Ai position and the forward right hit position
            rB2D.AddForce(-transform.right * avoidSpeed);// Add force to the left
        }
        else // If the cast no is no longer hitting anything the continue
        {
            rB2D.velocity = Vector3.zero;// Stops all rigidbody movement, this removes any sliding around
        }
    }

    private GameObject FindLastPosition()
    {
        lastPositions = GameObject.FindGameObjectsWithTag("Player"); // Find all game objects with a tag
        float distance = Mathf.Infinity; // set a float for an infinit distance this (this can be changed to a distance value)

        for (int i = 0; i < lastPositions.Length; i++) // For every lastPositions that was found with the above tag
        {
            RaycastHit hitLastPos;
            if (Physics.Raycast(transform.position, lastPositions[i].transform.position - transform.position, out hitLastPos)) // Cast a line from the Ai to the next lastPosition in sequence
            {
                if (hitLastPos.collider.tag == "Player") // If the raycast hits a game object with a tag then continue
                {
                    Vector2 diff = lastPositions[i].transform.position - Player.position; // Set a vector distance from the last position to the Player
                    float curDistance = diff.sqrMagnitude; // Apply squared magnitude to the vector for distance comparison
                    if (curDistance < distance) // If the current distance from the last position is less than the set distance
                    {
                        closestPosition = lastPositions[i]; // Set the closestPosition game object to equal the last position that has been found
                        distance = curDistance; // Set the distance to the current distance
                    }
                }
            }
        }
        return closestPosition; // Return the closestPosition game object that was found
    }
    */
}
