﻿using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
//Script Based on and Referenced From FPS -Raycasting https://www.youtube.com/watch?v=KPdP3CdzUmk&index=11&list=PLmM3JTiGlvJIMDbczqaEs5K9XrRdtQBrl
public class PlayerRaycasting : MonoBehaviour
{
    /**********************************************/
    /*This Script is for the player to interact   */
    /* With the Environment.                      */
    /**********************************************/
    
    public float distanceToSee = 4;
    RaycastHit whatIHit;
    public GameObject player;
    
    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
        Debug.DrawRay(this.transform.position,this.transform.forward * distanceToSee, Color.magenta); //Access the starting positiioon of the Raycast
        
        if (Physics.Raycast(this.transform.position, this.transform.forward, out whatIHit, distanceToSee))
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                /***********************/
                /*Red key and Red Door */
                /***********************/
               //Tell player what they picked up in Debug log 
               Debug.Log("I picked up a " + whatIHit.collider.gameObject.name);
               if (whatIHit.collider.tag == "Keyswitch") //if the object touched matches the tag Keyswicth
               {

                   if (whatIHit.collider.gameObject.GetComponent<KeySwitch>().whatKeyAmI == KeySwitch.Keyswitch.RedKey) //Access the keySwitch componenet on the key to store what kind of key it is.
                   {
                       player.GetComponent<Inventory>().hasRedKey = true; //This is accessing the player components so we can check inventory
                       Destroy(whatIHit.collider.gameObject); //Destroy key object
                   }
               }

               if (whatIHit.collider.tag == "Doors") //if the object touched matches the tag Keyswicth
               {

                   if (whatIHit.collider.gameObject.GetComponent<DoorManager>().whatDoorAmI == DoorManager.Doors.RedDoor) //Access the keySwitch componenet on the key to store what kind of key it is.
                   {
                      if (player.GetComponent<Inventory>().hasRedKey)
                       {
                           player.GetComponent<Inventory>().hasRedKey = false; //This is accessing the player components so we can check inventory
                           whatIHit.collider.gameObject.transform.Rotate(0, -90, 0);
                           Destroy(whatIHit.collider.gameObject); //Destroy Door object
                       }
                       else
                       {
                           Debug.Log("Find the Red Key!"); //Tell user to find the key to the door 
                       }
                   }
               }
                /*********************************/
                /*Turqoise key and Turqoise Door */
                /*********************************/
                //Tell player what they picked up in Debug log 
                Debug.Log("I picked up a " + whatIHit.collider.gameObject.name);
               if (whatIHit.collider.tag == "Keyswitch") //if the object touched matches the tag Keyswicth
               {

                   if (whatIHit.collider.gameObject.GetComponent<KeySwitch>().whatKeyAmI == KeySwitch.Keyswitch.TurqoiseKey) //Access the keySwitch componenet on the key to store what kind of key it is.
                   {
                       player.GetComponent<Inventory>().hasTurqoiseKey = true; //This is accessing the player components so we can check inventory
                       Destroy(whatIHit.collider.gameObject); //Destroy key object
                   }
               }

               if (whatIHit.collider.tag == "Doors") //if the object touched matches the tag Keyswicth
               {

                   if (whatIHit.collider.gameObject.GetComponent<DoorManager>().whatDoorAmI == DoorManager.Doors.TurqoiseDoor) //Access the keySwitch componenet on the key to store what kind of key it is.
                   {
                      if (player.GetComponent<Inventory>().hasTurqoiseKey)
                       {
                           player.GetComponent<Inventory>().hasTurqoiseKey = false; //This is accessing the player components so we can check inventory
                           Destroy(whatIHit.collider.gameObject); //Destroy Door object
                       }
                       else
                       {
                           Debug.Log("Find the Turqoise Key!"); //Tell user to find the key to the door 
                       }
                   }
               }               
                /*****************************/
                /*Yellow key and Yellow Door */
                /*****************************/
               //Tell player what they picked up in Debug log 
               Debug.Log("I picked up a " + whatIHit.collider.gameObject.name);
               if (whatIHit.collider.tag == "Keyswitch") //if the object touched matches the tag Keyswicth
               {

                   if (whatIHit.collider.gameObject.GetComponent<KeySwitch>().whatKeyAmI == KeySwitch.Keyswitch.YellowKey) //Access the keySwitch componenet on the key to store what kind of key it is.
                   {
                       player.GetComponent<Inventory>().hasYellowKey = true; //This is accessing the player components so we can check inventory
                       Destroy(whatIHit.collider.gameObject); //Destroy key object
                   }
               }

               if (whatIHit.collider.tag == "Doors") //if the object touched matches the tag Keyswicth
               {

                   if (whatIHit.collider.gameObject.GetComponent<DoorManager>().whatDoorAmI == DoorManager.Doors.YellowDoor) //Access the keySwitch componenet on the key to store what kind of key it is.
                   {
                      if (player.GetComponent<Inventory>().hasYellowKey)
                       {
                           player.GetComponent<Inventory>().hasYellowKey = false; //This is accessing the player components so we can check inventory
                           Destroy(whatIHit.collider.gameObject); //Destroy Door object
                       }
                       else
                       {
                           Debug.Log("Find the Yellow Key!"); //Tell user to find the key to the door 
                       }
                   }
               }
           }
       }

   }
}
