﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reveal : MonoBehaviour
    /* when the player discovers an object they will be able to reveal the object by destorying, its mask  that hides the object*/
{
    public GameObject cameraOne, cameraTwo; //the two cams to swicth between 

    AudioListener cameraOneAudioLis;
    AudioListener cameraTwoAudioLis;

    //int for which cam is on 
   public int whichCamIsOn = 0;

    // Use this for initialization
    void Start()
    {

        //Get Camera Listeners
        cameraOneAudioLis = cameraOne.GetComponent<AudioListener>();
        cameraTwoAudioLis = cameraTwo.GetComponent<AudioListener>();

        //cam one the normal cam is active 
        whichCamIsOn = 1;
        cameraOne.SetActive(true);
        cameraOneAudioLis.enabled = true;

        //cam two the soul cam is set to false 
        cameraTwo.SetActive(false);
        cameraTwoAudioLis.enabled = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R)) //when the Player Presses R Switch to soul vision 
        {
            whichCamIsOn++;
            switchCamera();
        }
    }

    //Change from Main Camera to soul camera 
    void switchCamera()
    {
        if (whichCamIsOn > 2)
        {
            whichCamIsOn = 1;
        }

        if (whichCamIsOn == 1)
        {
            //Main camera On Soul Cam off
            cameraOne.SetActive(true);
            cameraOneAudioLis.enabled = true;

            cameraTwoAudioLis.enabled = false;
            cameraTwo.SetActive(false);
        }

        else if (whichCamIsOn == 2)
        {
            cameraTwo.SetActive(true);
            cameraTwoAudioLis.enabled = true;

            cameraOneAudioLis.enabled = false;
            cameraOne.SetActive(false);
        }

        /*
            switch (whichCamIsOn)
            {
                case 1: //Main camera On Soul Cam off
                    cameraOne.SetActive(true);
                    cameraOneAudioLis.enabled = true;

                    cameraTwoAudioLis.enabled = false;
                    cameraTwo.SetActive(false);
                    break;

                case 2: //Soul Cam on Main Cam off 
                    cameraTwo.SetActive(true);
                    cameraTwoAudioLis.enabled = true;

                    cameraOneAudioLis.enabled = false;
                    cameraOne.SetActive(false);
                    break;
            }
            */
    }
}
