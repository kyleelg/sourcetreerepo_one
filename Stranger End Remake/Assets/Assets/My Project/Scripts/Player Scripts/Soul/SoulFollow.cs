﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//The purpose of this script is to have the player's helping soul follow them. 
public class SoulFollow : MonoBehaviour
{
    /**********************************************************************************/
    /*This Script allows a game object to follow and circle around some transform point*/
    /*Any Transform can be set as the target                                           */
    /**********************************************************************************/

    Transform target; //What ever the object is following
    float moveSpeed = 5; //The movement speed of the obeject 
    float rotationSpeed = 1f; //The speed at which the object rotate around player
    Transform myTransform; //The soul Objects Transform Position 

    void Awake() { myTransform = transform; }
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindWithTag("SoulFollow").transform; //Find the player 
    }

    // Update is called once per frame
    void Update()
    {
        myTransform.rotation = Quaternion.Slerp(myTransform.rotation,
        Quaternion.LookRotation(target.position - myTransform.position), rotationSpeed * Time.deltaTime);
        myTransform.position += myTransform.forward * moveSpeed * Time.deltaTime;
    }
}
/*Instead of setting the player as the target you should set a child of an empty GameObject to it, move it where you want the cube to float and use that as a target.*/

