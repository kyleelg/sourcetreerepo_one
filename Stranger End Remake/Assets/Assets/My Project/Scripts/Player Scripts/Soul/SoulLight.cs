﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//This scripting will control the light source for the players guidance system.
//This light will grow and shrink in brightness and intensity, depending on how close it is to the object it is detecting.   
public class SoulLight : MonoBehaviour
{
    /**********************************************************************************/
    /*The Intensity of a light is multiplied with the Light color.                    */
    /*The value can be between 0 and 8. This allows you to create over bright lights. */
    /**********************************************************************************/

    // Interpolate light color between two colors back and forth
    float duration = 1.0f;
    Color color0 = Color.yellow;
    Color color1 = Color.blue;
    Color color2 = Color.magenta;
    Light SoulLT;       //Light source of gameobject 
    GameObject[] player;   //Find player game object tag.
    Collider[] Detection; //Physics overlap detection
    void Start()
    {
        SoulLT = GetComponent<Light>();
        player = GameObject.FindGameObjectsWithTag("Player");
    }

    void Update()
    {
        //Every Frame check for a Detection.
        Collider[] Detection = Physics.OverlapSphere(transform.position, 50); //Soul looks for any physics overlap

        foreach (Collider hit in Detection) //if an overlap is detected then Check if it is a key or enemy 
        {
            if (hit.tag == "Keyswitch")
            {
                FindingKey();
            }

            if (hit.tag == "Enemy")
            {
                WarnPlayer();
            }

            else { return; }
        }

    }
    //When player is near a key or enemy increase intensity as they get closer and decrease intensity as the get further.
    void FindingKey()
    {
        // Darken the light completely over a period of 2 seconds.
        // set light color to blue 
        float t = Mathf.PingPong(Time.time, duration) / duration;
        SoulLT.color = Color.Lerp(color0, color1, t);

        //change color of light to green 
        //gradually increase in tensity as gets closer to key source 
    }

    void WarnPlayer()
    {
        // Darken the light completely over a period of 2 seconds.
        // set light color to magenta 
        float t = Mathf.PingPong(Time.time, duration) / duration;
        SoulLT.color = Color.Lerp(color0, color2, t);

        //change color of light to purple 
        //gradually increase in tensity as gets closer to the enemy source 
    }
}
